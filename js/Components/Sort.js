
class Sort {
    constructor (data) {
        this.data = {
            alphabet: 'unset',
            gender: 'unset',
        };
        this.domGenderSort = null;
        this.domAlphabedSort = null;
        this.onChange = data.onChange;
    }

    update () {

    }

    initEvents () {
        this.domGenderSort = document.getElementById('gender-sort');
        this.domAlphabedSort = document.getElementById('alphabet-sort');

        this.domGenderSort.addEventListener('change', () => {
            this.data.gender = this.domGenderSort.value;
            
            this.onChange(this.data);
        })

        this.domAlphabedSort.addEventListener('change', () => {
            this.data.alphabet = this.domAlphabedSort.value;
            
            this.onChange(this.data);
        })
    }

    get template () {
        return `
            <div class="sort-block">
                <div class="sort-block__item">
                    <div class="mb-1">Sort by gender</div>
                    <select id="gender-sort">
                        <option value="unset">Unset</option>
                        <option value="genderMales">Males first</option>
                        <option value="genderFemales">Females first</option>
                    </select>
                </div>

                <div class="sort-block__item">
                    <div class="mb-1">Sort by alphabet</div>
                    <select id="alphabet-sort">
                        <option value="unset">Unset</option>
                        <option value="alphabetUp">Alphabet Up</option>
                        <option value="alphabetDown">Alphabet Down</option>
                    </select>
                </div>
            </div>
        `
    }
}
