
class Statistics {
    constructor (data) {
        this.data = data;

        this.statistics = {
            total: 0,
            genders: {
                male: 0,
                female: 0
            },
            nationality: {},
            telegram: 0, // WTF? TAKOGO NETU NIUKOGO
        };
    }

    setStatistics () {
        let total = 0;
        let males = 0;
        let females = 0;
        let nationality = {};

        this.data.forEach((el) => {
            total++;
            el.gender === 'male' ? males++ : females++;
            if (!nationality[el.nat]) {
                nationality[el.nat] = 0;
            }
            nationality[el.nat] += 1;
        })

        this.statistics.total = total;
        this.statistics.genders = {
            male: males,
            female: females
        };
        this.statistics.nationality = nationality;
    }

    clearStatistics () {
        this.statistics.total = 0;
        this.statistics.genders = {
            male: 0,
            female: 0
        };
        this.statistics.nationality = {};
        this.statistics.telegram = 0;
    }

    update (data) {
        this.data = data;

        this.clearStatistics();
        this.setStatistics();
    }

    get gendersAdvantage () {
        return this.statistics.male > this.statistics.female ? 'Males' : 'Females';
    }

    get nationalityTemplate () {
        let bus = '';

        for (const key in this.statistics.nationality) {
            const element = this.statistics.nationality[key];

            bus += `<div><b>${key}</b> = ${element}</div>`
        }

        return bus;
    }

    get template () {
        return `
            <div class="statistics-block">
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">Total: </div>
                    <div class="text-center">${this.statistics.total}</div>
                </div>
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">Total males: </div>
                    <div class="text-center">${this.statistics.genders.male}</div>
                </div>
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">Total females: </div>
                    <div class="text-center">${this.statistics.genders.female}</div>
                </div>
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">Quantity advantage: </div>
                    ${this.gendersAdvantage} has quantity advantage
                </div>
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">Nationality: </div>
                    ${this.nationalityTemplate}
                </div>
                <div class="statistics-block__item mb-2">
                    <div class="mb-2 title-sm">TELEGRAM: </div>
                    <div class="text-center">${this.statistics.telegram}</div>
                </div>
            </div>
        `
    }
}
