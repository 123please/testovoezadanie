
class Filter {
    constructor (data) {
        this.data = {
            males: true,
            females: true,
        };
        this.search = '';
        this.domSearchFilter = null;
        this.onChange = data.onChange;
    }

    update () {

    }

    initEvents () {
        this.domSearchFilter = document.getElementById('search-filter');
        this.domMalesFilter = document.getElementById('filter-males');
        this.domFemalesFilter = document.getElementById('filter-females');

        this.domSearchFilter.addEventListener('keyup', () => {
            this.search = this.domSearchFilter.value;
            
            this.onChange(this.data, this.search);
        })

        this.domMalesFilter.addEventListener('change', () => {
            this.data.males = this.domMalesFilter.checked;
            
            this.onChange(this.data, this.search);
        });

        this.domFemalesFilter.addEventListener('change', () => {
            this.data.females = this.domFemalesFilter.checked;
            
            this.onChange(this.data, this.search);
        });
    }

    get template () {
        return `
            <div class="sort-block">
                <div class="sort-block__item">
                    <div class="mb-1">Search (First name/Last name/Phone/Mail)</div>
        
                    <input id="search-filter" value="" />
                </div>

                <div class="sort-block__item">
                    <div class="mb-1">Filter by gender</div>

                    <label>
                        <span>Males: </span>

                        <input id="filter-males" type="checkbox" checked>
                    </label>

                    <label>
                        <span>Females: </span>

                        <input id="filter-females" type="checkbox" checked>
                    </label>
                </div>
            </div>
        `
    }
}
