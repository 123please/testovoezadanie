class Cards {
    constructor (data) {
        this.data = data;
        this.cards = [];
    }

    get template () {
        let bus = '';

        if (this.cards.length) {
            this.cards.forEach((el) => {
                bus += el.template;
            })
        }

        return bus;
    }

    setCards () {
        this.data.forEach((el) => {
            this.cards.push(new Card(el));
        })
    }

    clearCards () {
        this.cards = [];
    }

    update (data) {
        this.data = data;

        this.clearCards();
        this.setCards();
    }
}
