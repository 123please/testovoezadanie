
class Card {
    constructor (data) {
        this.data = {
            picture: {
                large: ''
            },
            name: {
                title: '',
                first: '',
                last: ''
            },
            phone: '',
            email: '',
            ...data
        };
    }

    get parsedPhone () {
        let phone = this.data.phone.replace(/\+/g, '').replace(/-/g, '').replace(/ /g, '').replace(/\(/g, '').replace(/\)/g, '');
        let f = phone.slice(0, 1);
        let n = phone.slice(1, 4);
        let c1 = phone.slice(4, 6);
        let c2 = phone.slice(6, 8);
        let l = phone.slice(-3);

        return `+${f} (${n}) ${c1} - ${c2} - ${l}`
    }

    get parsedAddres () {
        return `${this.data.location.state} / ${this.data.location.city} / ${this.data.location.street.name}, ${this.data.location.street.number}`
    }

    get mapLocation () {
        return `https://www.google.com/maps/@${this.data.location.coordinates.latitude},${this.data.location.coordinates.longitude},14z`
    }

    get template () {
        return `
            <div class="card">
                <div class="card__top-row mb-3">
                    <div class="card__image mr-3">
                        <img src="${this.data.picture.large}" alt="" title="">
                    </div>

                    <div class="pt-2 pb-3 flex column">
                        <div class="card__title mb-1">
                            ${this.data.name.title} ${this.data.name.first} ${this.data.name.last}
                        </div>

                        <span class="mt-auto">Phone:</span>
                        <a href="tel:${this.data.phone}" class="card__text-item">
                            ${this.parsedPhone}
                        </a>
                    </div>
                </div>

                <div class="card__bottom-row">
                    <span>Email:</span>
                    <a href="mailto:${this.data.email}" class="card__text-item mb-2">
                        ${this.data.email}
                    </a>
    
                    <span>Addres:</span>
                    <a href="${this.mapLocation}" target="_blank" class="card__text-item mb-2">
                        ${this.parsedAddres}
                    </a>
    
                    <span>Birthdate:</span>
                    <div class="card__text-item mb-2">
                        ${new Date(this.data.dob.date).toDateString()}
                    </div>
    
                    <span>Registered:</span>
                    <div class="card__text-item mb-2">
                        ${new Date(this.data.registered.date).toDateString()}
                    </div>
                </div>
            </div>
        `
    }
}
