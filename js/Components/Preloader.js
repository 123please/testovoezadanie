
class Preloader {
    constructor (node) {
        this.node = node;
    }

    start () {
        this.preloader = document.createElement('div');

        this.preloader.classList.add('preloader');

        this.node.appendChild(this.preloader);
    }

    stop () {
        this.preloader.remove();
    }
}
