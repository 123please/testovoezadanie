
class Pagination {
    constructor (data) {
        this.data = data.data;
        this.perPage = 10;
        this.currentPage = 1;

        this.domPerPage = null;
        this.domPaginationButtons = null;
        this.onChange = data.onChange;
    }

    update (data) {
        this.data = data;
    }

    get totalPages () {
        return this.perPage != 'all' ? Math.ceil(this.data.length / this.perPage) : 999999;
    }

    initEvents () {
        this.domPerPage = document.getElementById('per-page-select');
        this.domPaginationButtons = document.querySelectorAll('[data-pagination-button]');

        this.domPerPage.addEventListener("change", () => {
            this.perPage = this.domPerPage.value == 'all' ? 999999 : Number(this.domPerPage.value);

            this.pagesFix();

            this.onChange({
                perPage: this.perPage,
                currentPage: this.currentPage,
                totalPages: this.totalPages
            });
        })

        this.domPaginationButtons.forEach((el) => {
            el.addEventListener('click', () => {
                if (el.getAttribute('data-pagination-button') == 'first') {
                    this.currentPage = 1;
                }
                else if (el.getAttribute('data-pagination-button') == 'prev') {
                    if (this.currentPage > 1) {
                        this.currentPage--;
                    }
                }
                else if (el.getAttribute('data-pagination-button') == 'next') {
                    if (this.currentPage < this.totalPages) {
                        this.currentPage++;
                    }
                }
                else if (el.getAttribute('data-pagination-button') == 'last') {
                    this.currentPage = this.totalPages;
                } else {
                    this.currentPage = Number(el.getAttribute('data-pagination-button'));
                }

                this.pagesFix();

                this.onChange({
                    perPage: this.perPage,
                    currentPage: this.currentPage,
                    totalPages: this.totalPages
                });
            })
        })
    }

    buttonTemplate (index) {
        return `<button data-pagination-button="${index}" class="${index == this.currentPage ? 'is-active' : ''}">${index}</button>`;
    }

    pagesFix () {
        if (this.currentPage > this.totalPages) {
            this.currentPage = this.totalPages;
        }
    }

    get buttons () {
        let bus = '';

        for (let index = 1; index < this.totalPages + 1; index++) {
            bus += this.buttonTemplate(index);
        }

        return bus;
    }

    get template () {
        return `
            <div class="pagination-counter mt-4">
                Per page:&nbsp;
                <select id="per-page-select">
                    <option value="5" ${this.perPage == 5 ? 'selected' : ''}>5</option>
                    <option value="10" ${this.perPage == 10 ? 'selected' : ''}>10</option>
                    <option value="20" ${this.perPage == 20 ? 'selected' : ''}>20</option>
                    <option value="all" ${this.perPage == 999999 ? 'selected' : ''}>All</option>
                </select>
            </div>

            <div class="pagination">
                <button data-pagination-button="first"><<</button>
                <button data-pagination-button="prev"><</button>
                
                ${this.buttons}
                
                <button data-pagination-button="next">></button>
                <button data-pagination-button="last">>></button>
            </div>
        `
    }
}
