const sorts = {
    gender: {
        genderFemales: (data) => {
            return data.sort((a, b) => {
                if (a.gender == 'male' && b.gender == 'female') {
                    return 1;
                } else if (a.gender == 'female' && b.gender == 'male') {
                    return -1;
                } else {
                    return 0;
                }
            });
        },
        
        genderMales: (data) => {
            return data.sort((a, b) => {
                if (a.gender == 'female' && b.gender == 'male') {
                    return 1;
                } else if (a.gender == 'male' && b.gender == 'female') {
                    return -1;
                } else {
                    return 0;
                }
            });
        }
    },

    alphabet: {
        alphabetUp: (data) => {
            return data.sort((a, b) => {
                if (a.name.first.charAt(0) > b.name.first.charAt(0)) {
                    return 1;
                } else if (a.name.first.charAt(0) < b.name.first.charAt(0)) {
                    return -1;
                } else {
                    return 0;
                }
            });
        },

        alphabetDown: (data) => {
            return data.sort((a, b) => {
                if (a.name.first.charAt(0) < b.name.first.charAt(0)) {
                    return 1;
                } else if (a.name.first.charAt(0) > b.name.first.charAt(0)) {
                    return -1;
                } else {
                    return 0;
                }
            });
        }
    }
}