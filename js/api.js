
class Api {
    constructor (data) {
        this.headers = { 
            "Content-Type": "application/json; charset=utf-8" 
        }
    }

    _handle (url, method, data) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: method,
                headers: this.headers,
                body: data ? JSON.stringify(data) : null
            })
            .then(res => res.json())
            .then(response => {
                resolve(response);
            })
            .catch(err => {
                reject(err);
            });
        });
    }

    get (url, data) {
        return new Promise((resolve, reject) => {
            this._handle(url, 'get', data).then((resp) => {
                resolve(resp);
            }, (err) => {
                reject(err);
            });
        });
    }

    post (url, data) {
        return new Promise((resolve, reject) => {
            this._handle(url, 'post', data).then((resp) => {
                resolve(resp);
            }, (err) => {
                reject(err);
            });
        });
    }
}
