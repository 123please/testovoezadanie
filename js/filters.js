const filters = {
    males: (data, value) => {
        return data.filter((el) => {
            if (value) {
                if (el.gender == 'male') {
                    return true;
                }
            } else {
                if (el.gender == 'male') {
                    return false;
                }
            }
        });
    },

    females: (data, value) => {
        return data.filter((el) => {
            if (value) {
                if (el.gender == 'female') {
                    return true;
                }
            } else {
                if (el.gender == 'female') {
                    return false;
                }
            }
        });
    }
}

const search = (data, value) => {
    return data.filter((el) => {
        if (
            el.name.first.indexOf(value) != -1 ||
            el.name.last.indexOf(value) != -1 ||
            el.email.indexOf(value) != -1 ||
            el.phone.indexOf(value) != -1
        ) {
            return true
        } else {
            return false
        }
    })
}