
class App {
    constructor (data) {
        this._data = data.data;
        this.data = this._data;
        this.sorts = {};
        this.filter = {};
        this.search = '';
        this.pagination = {
            perPage: 10,
            currentPage: 1
        };
        this.page = [];

        this.cardsRenderer = data.cardsRenderer;
        this.cardsComponent = new Cards(this._data);

        this.statisticsRenderer = data.statisticsRenderer;
        this.statisticsComponent = new Statistics(this._data);

        this.paginationRenderer = data.paginationRenderer;
        this.paginationComponent = new Pagination({
            data: this._data,
            onChange: (data) => {
                this.pagination = data;

                this.updateData();
            }
        });

        this.filterRenderer = data.filterRenderer;
        this.filterComponent = new Filter({
            onChange: (filter, search) => {
                this.filter = filter;
                this.search = search;

                this.updateData();
            }
        });

        this.sortRenderer = data.sortRenderer;
        this.sortComponent = new Sort({
            onChange: (sorts) => {
                this.sorts = sorts;

                this.updateData();
            }
        });

        this.renderCards();
        this.renderStatistics();
        this.renderSort();
        this.renderFilter();
        this.renderPagination();
    }

    updateData (data = this.data) {
        this.data = data;
        this._data = data;

        if (this.search.length) {
            this._data = search(this._data, this.search);
        }

        if (Object.keys(this.filter).length) {
            this.filterHandler();
        }

        if (Object.keys(this.sorts).length) {
            this.sortHandler();
        }
        
        if (this.pagination.currentPage) {
            this.paginationHandler();
        }

        this.renderCards();
        this.renderPagination();
        this.renderStatistics();
    }

    renderCards () {
        this.cardsComponent.update(this.page);

        this.cardsRenderer.innerHTML = this.cardsComponent.template;
    }

    renderStatistics () {
        this.statisticsComponent.update(this._data);

        this.statisticsRenderer.innerHTML = this.statisticsComponent.template;
    }

    renderFilter () {
        this.filterComponent.update(this._data);

        this.filterRenderer.innerHTML = this.filterComponent.template;

        this.filterComponent.initEvents();
    }

    renderSort () {
        this.sortComponent.update(this._data);

        this.sortRenderer.innerHTML = this.sortComponent.template;

        this.sortComponent.initEvents();
    }

    renderPagination () {
        this.paginationComponent.update(this._data);

        this.paginationRenderer.innerHTML = this.paginationComponent.template;

        this.paginationComponent.initEvents();
    }

    paginationHandler () {
        console.log(this.pagination)
        this.page = this._data.slice((this.pagination.currentPage - 1) * this.pagination.perPage, this.pagination.currentPage * this.pagination.perPage);
    }

    filterHandler () {
        let bus = [];

        for (const key in this.filter) {
            const value = this.filter[key];

            bus = bus.concat(filters[key](this._data, value));
        }

        this._data = bus;
    }

    sortHandler () {
        for (const key in this.sorts) {
            const by = this.sorts[key];

            if (by != 'unset') {
                this._data = sorts[key][by](this._data);
            }
        }
    }
}
